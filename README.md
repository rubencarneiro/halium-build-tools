# Halium Build Tools for Jenkins

This repository contains scripts and a Jenkins shared pipeline library for building Halium device compatibility images. It is provided **without guarantee of stability for downstream projects**. That is, if you're using this repository as a shared library on your own Jenkins instance, things might break without warning. If you're using this on your own server, please fork this repository and use it as needed.

Now that the disclaimer is out of the way, here's how you'll use it:

## Deployment

This repository expects to be deployed at `/opt/halium_build/` on the Jenkins worker node that will be running the shared library's jobs. The directory should be owned by your Jenkins user if you plan to make Jenkins do the `git pull` to update the repository. Otherwise, the directory should be owned by `root` with `u=rwx,g=rx,o=rx` permissions.

Once the repository is deployed, the repo trees for building must be initialized:

```sh
mkdir /opt/halium_build/5.1/ && cd /opt/halium_build/5.1/ && repo init -u https://github.com/Halium/android -b halium-5.1 --depth=1
mkdir /opt/halium_build/7.1/ && cd /opt/halium_build/7.1/ && repo init -u https://github.com/Halium/android -b halium-7.1 --depth=1
mkdir /opt/halium_build/9.0/ && cd /opt/halium_build/9.0/ && repo init -u https://github.com/Halium/android -b halium-9.0 --depth=1
```

Make sure the user your Jenkins worker is running as has permission to write to these directories.

At this point, you can build a Halium device compatibility image using the `build.sh` script and package them into `system-image` compatible tarballs with `build-tarball.sh`.

Once the system is prepared, you're ready to add the shared library to your Jenkins instance. The shared library can be used in Jenkins Configuration as Code with a configuration similar to the following:

```yaml
jenkins:
  labelAtoms:
  - name: "halium-5.1"
  - name: "halium-7.1"
  - name: "halium-9.0"
  throttleJobProperty:
    categories:
    - categoryName: "halium-9.0"
      maxConcurrentPerNode: 1
      maxConcurrentTotal: 0
    - categoryName: "halium-7.1"
      maxConcurrentPerNode: 1
      maxConcurrentTotal: 0
    - categoryName: "halium-5.1"
      maxConcurrentPerNode: 1
      maxConcurrentTotal: 0
  gitLabServers:
    servers:
    - credentialsId: "<YourSuperCoolCredential>"
      manageWebHooks: true
      name: "main-gitlab"
      serverUrl: "https://gitlab.com"
  globalLibraries:
    libraries:
    - defaultVersion: "main"
      includeInChangesets: false
      name: "ubports-halium-build-tools"
      retriever:
      modernSCM:
        scm:
        gitLab:
          id: "<guid>"
          projectOwner: "ubports/community-ports/jenkins-ci"
          projectPath: "ubports/community-ports/jenkins-ci/halium-build-tools"
          serverName: "main-gitlab"
          traits:
          - gitLabBranchDiscovery:
            strategyId: 1
```

But again, please fork this repository so changes for our infrastructure do not break yours.

Label your build node with a `halium-<version>` label for each version of Halium you would like to build on that server.

Once your Jenkins instance is prepared, you can use [Jenkinsfile-example](Jenkinsfile-example) to create your own jobs. Make sure the devices you're building for are in the halium-devices repo!

## FAQ

### Why don't you make a separate repo tree for each job?

We aren't made of SSDs, nor are we made of bandwidth. Making the single tree for each Halium version was the most straightforward way to build all these jobs without taking a bunch of time to sync the repo tree.

Of course, this violates Jenkins conventions by not doing an actual build inside the allocated workspace. If you can think of a way to make this work in a way that can remove its repo tree (saving resting disk space) while also not needing to redownload the tree every time (saving bandwidth), we're all ears.
