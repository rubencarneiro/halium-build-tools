#!/bin/bash
set -ex

device="$1"
output="$2"
device_dir="$3"
build_dir="$4"
dir="$build_dir/out/target/product/$device"

echo "Working on device: $device"
if [ ! -f "$dir/boot.img" ]; then
    echo "boot.img does not exist!"
exit 1; fi
if [ ! -f "$dir/recovery.img" ]; then
    echo "recovery.img does not exist!"
exit 1; fi
if [ ! -f "$dir/system.img" ]; then
    echo "system.img does not exist!"
exit 1; fi
wDir=$(mktemp -d /tmp/ota.XXXXXXXX)
mkdir "$wDir/partitions"
cp "$dir/boot.img" "$wDir/partitions"
cp "$dir/recovery.img" "$wDir/partitions"

## This is the new overlay used for partitions, firmware, etc.
cp -r "$build_dir/$device_dir"/ubuntu-overlay/* "$wDir/" || true
cp -r "$build_dir/$device_dir"/ubuntu_overlay/* "$wDir/" || true

mkdir -p "$wDir/system/var/lib/lxc/android/"
cp "$dir/system.img" "$wDir/system/var/lib/lxc/android/"

# Create legacy channel flag, forces system.img hardlink creation
echo "Legacy channel flag for creating system.img hardlink, for ubp-5.1 boot images only" > "$wDir/legacy_channel"

tar cfJ "$output/device_${device}_devel.tar.xz" -C "$wDir" partitions/ system/ legacy_channel
echo "$(date +%Y%m%d)-$RANDOM" > "$output/device_${device}_devel.tar.build"

# Also move recovery and bootimg to outdir
cp "$dir/recovery.img" "$output/ubp-unlocked-recovery_${device}.img"
cp "$dir/boot.img" "$output/ubp-boot_${device}.img"

rm -r "$wDir"
