#!/bin/bash
set -ex

device="$1"
output="$2"
ver="$3"
iscombined="$4"
dir="out/target/product/$device/images"

if [[ "$device" == halium_* ]]; then
    bootimage=''
elif [ "$iscombined" = "true" ]; then
    bootimage=boot.img
else
    bootimage=halium-boot.img
fi

cd "/opt/halium_build/$ver"

echo "Working on device: $device"

if [ -n "$bootimage" ] && [ ! -f "$dir/$bootimage" ]; then
    echo "$bootimage does not exist!"
    exit 1;
fi

if [ "$iscombined" != "true" ]; then
    if [ ! -f "$dir/recovery.img" ]; then
        echo "recovery.img does not exist!"
        exit 1;
    fi
fi

if [ ! -f "$dir/system.img" ]; then
    echo "system.img does not exist!"
    exit 1;
fi

wDir=$(mktemp -d /tmp/ota.XXXXXXXX)
mkdir "$wDir/partitions"
if [ -n "$bootimage" ]; then
    cp "$dir/$bootimage" "$wDir/partitions/boot.img"
fi
# Do not copy recovery.img on Volla Phone until Halium 9 recovery performs same as current built recovery
if [ -f "$dir/recovery.img" ] && [ "$iscombined" != "true" ] && [ "$device" != "yggdrasil" ]; then
    echo "recovery image found"
    cp "$dir/recovery.img" "$wDir/partitions"
fi

if [ -f "$dir/dtbo.img" ]; then
    echo "dtbo image found"
    cp "$dir/dtbo.img" "$wDir/partitions"
fi

if [ -f "$dir/vbmeta.img" ]; then
    echo "vbmeta image found"
    cp "$dir/vbmeta.img" "$wDir/partitions"
fi

if [ -f "$dir/vendor.img" ]; then
    echo "vendor image found"
    cp "$dir/vendor.img" "$wDir/partitions"
fi

# Copy common device-files first so if there is some device spesific changes it will override the common ones
mkdir -p "$wDir/system/var/lib/lxc/android/"

## SPARSE FILE TRANSLATION
# Needed with Halium-7.1 builds mostly
fileType=$(file -b0 "$dir/system.img")
if [[ $fileType == "Android sparse image"* ]]; then
    echo "Converting sparse image to image"
    mv "$dir/system.img" "$dir/system.sparse.img"
    simg2img "$dir/system.sparse.img" "$dir/system.img"
    e2fsck -fy "$dir/system.img" || true
    resize2fs -p -M "$dir/system.img"
fi

# ver >= 9
if [ "$ver" = "9.0" ] || [[ "$ver" = "1"* ]]; then
    cp "$dir/system.img" "$wDir/system/var/lib/lxc/android/android-rootfs.img"
else
    cp "$dir/system.img" "$wDir/system/var/lib/lxc/android/"
fi
tar cf "$output/halium_${device}.tar" -C "$wDir" partitions/ system/
xz --threads=0 -1 "$output/halium_${device}.tar"
echo "$(date +%Y%m%d)-$RANDOM" > "$output/halium_${device}.tar.build"

# Also move recovery and bootimg to outdir
if [ "$iscombined" != "true" ]; then
    cp "$dir/recovery-unlocked.img" "$output/halium-unlocked-recovery_${device}.img"
    cp "$dir/recovery.img" "$output/halium-locked-recovery_${device}.img"
fi
if [ -n "$bootimage" ]; then
    cp "$dir/$bootimage" "$output/halium-boot_${device}.img"
fi

rm -rf ./out/
rm -r "$wDir"
